<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImageModel extends Model
{
    use HasFactory;

    protected  $table = 'images';

    protected $dates = [
        'updates_at',
        'created_at'
    ];

    protected $fillable = [
        'user_id',
        'image',
    ];






}
