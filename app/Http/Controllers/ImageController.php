<?php

/*
|--------------------------------------------------------------------------
| Доступ по ролям
|--------------------------------------------------------------------------
|  PU - публичные
|  PO - только приватные
|  AU - только авторизованные
*/


namespace App\Http\Controllers;

class ImageController extends Controller
{
    public function __invoke()
    {


        return view('image');
     
    }
}
