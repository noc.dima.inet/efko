<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    public function show(Request $request)
    {
        if(Auth::check()) {
            return redirect(route('private'));
        }
        return view('login');
    }

    public function login(Request $request)
    {
        if(Auth::check()) {
            return redirect(route('private'));
        }

        $formFields = $request->only(['email', 'password']);

        if(Auth::attempt($formFields)) {
            return redirect()->intended(route('private'));
        }
        
        return redirect()->intended(route('login'))
                         ->withErrors([
                            'email'    => 'Не удалось авторизоваться',
                            'password' => 'Пароль возможно не верный',
                         ]);

    }


}
