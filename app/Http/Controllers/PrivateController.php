<?php
/*
|--------------------------------------------------------------------------
| Доступ по ролям
|--------------------------------------------------------------------------
|  PU - публичные
|  PO - только приватные
|  AU - только авторизованные
*/
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\CountDocumentsController;
use Illuminate\Support\Facades\Auth;

use App\Models\ {
    ImageModel,
};

class PrivateController extends Controller
{
    public function __invoke(Request $request)
    {        
        $publicImages = ImageModel::select('image')->where('visible','=','PU')->get();
        $publicImages= $publicImages->pluck('image')->all();

        if( Auth::user()->name == 'admin' ) {
            $privateImages = ImageModel::select('image')
                                        ->whereNot('visible','=','PU')
                                        ->get();
        } else {
            $privateImages = ImageModel::select('image')
                                        ->where('visible','=','AU')
                                        ->get();
        }

        $privateImages = $privateImages->pluck('image')->all();



        return view('/private',[
            'name'            =>  Auth::user()->name,
            'publicImages'    => $publicImages,
            'privateImages'   => $privateImages,
            'countImgByDay'   => CountDocumentsController::countImgByDay(),
            'countImgByMonth' => CountDocumentsController::countImgByMonth(),
            'countImgByYears' => CountDocumentsController::countImgByYears(),

        ]);

    }
}
