<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use App\Models\ {
    ImageModel,
};



class IndexController extends Controller
{


    public function __invoke()
    {

        $publicImages = ImageModel::select('image')->where('visible','=','PU')->get();
        $publicImages= $publicImages->pluck('image')->all();

        return view('index', [
            'publicImages' => $publicImages,
        ]);
     
    }

}
