<?php

namespace App\Http\Controllers;
use App\Models\ {
    ImageModel,
};
use Illuminate\Http\Request;

class StoreImageController extends Controller
{
    public function __invoke(Request $request)
    {
        
        $visible  = $request->input('visible');
        $filename = $request->image->store('uploads');


        ImageModel::insert([
            'image'      => $filename,
            'visible'    => $visible,
            'date'       =>  date('Y:m:d'),
            'created_at' =>  now(),
        ]);

        return redirect('image');
         
    }
}
