<?php


/*
|--------------------------------------------------------------------------
| Методы для Статистики
|--------------------------------------------------------------------------
| Необходимо получить Соотношение публичных, 
| условно-приватных и приватных документов
| за выбранный интервал времени.
|  30 + 56 + 34 = 120 - сумма чисел.
|  1) (30/120)*100 = 25%.
|  PU - публичные
|  PO - только приватные
|  AU - только авторизованные
*/
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\CountDocumentsController;


class StatImageController extends Controller
{
    public function __invoke(Request $request)
    {
        $dateFrom  = $request->input('dateFrom');
        $dateTo    = $request->input('dateTo');

        $allDocsByRange = CountDocumentsController::allDocsByRange( $dateFrom, $dateTo );
      
        $totalRelationPU = CountDocumentsController::getDocsRelation( $dateFrom, $dateTo, 'PU'); // PU - публичные
        $totalRelationPO = CountDocumentsController::getDocsRelation( $dateFrom, $dateTo, 'PO'); // PO - только приватные
        $totalRelationAU = CountDocumentsController::getDocsRelation( $dateFrom, $dateTo, 'AU'); // AU - только авторизованные
        

        return view('/stat', [
            'dateFrom'      => $dateFrom,
            'dateTo'        => $dateTo,
            'allDocs'       => $allDocsByRange,
            'totalRelationPU'       => $totalRelationPU,
            'totalRelationPO'       => $totalRelationPO,
            'totalRelationAU'       => $totalRelationAU,
        ]);

        
    }
}
