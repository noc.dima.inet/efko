<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function __invoke(Request $request)
    {
        if(Auth::check()) {
            return redirect(route('private'));
        }

        $validateFields = $request->validate([
            'name'      => 'required|unique:users',
            'email'     => 'required|unique:users',
            'password'  => 'required',
        ]);

        $user = User::create($validateFields);

        if($user) {
            Auth::login($user);
            return redirect(route('private'));
        }

        return redirect(route('registration'))->withErrors([
            'forErrors' => 'Произошла ошибка при сохранении пользователя',
        ]);
    }
}
