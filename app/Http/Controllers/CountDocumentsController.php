<?php

/*
|--------------------------------------------------------------------------
| Методы для Суммирования документов
|--------------------------------------------------------------------------
| countImgByDay     - Сколько документов загружено в день 
| countImgByMonth   - Сколько документов загружено за месяц текущий 
| countImgByYear    - Сколько документов загружено за год текущий
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\ {
    ImageModel,
};

class CountDocumentsController extends Controller
{

    public static function countImgByDay(): int
    {
        return ImageModel::where('date', '=', date('Y-m-d'))->count();
    }

    public static function countImgByMonth(): int
    {
        return ImageModel::where('date', 'like', date('Y-m').'-%')->count();
    }

    public static function countImgByYears(): int
    {
        return ImageModel::where('date', 'like', date('Y-').'%-%')->count();
    }


    public static function allDocsByRange(string $date1, string $date2 ): int
    {
        return ImageModel::whereBetween('date', [$date1, $date2])->count();
    }

    public static function getDocsByVisibleRange(string $date1, string $date2, string $visible ): int
    {
        return ImageModel::whereBetween('date', [$date1, $date2])->where('visible', $visible )->count();
    }

    
    public static function getDocsRelation(string $date1, string $date2, string $visible ): int
    {
        // (30/120)*100 = 25%.
        $totalRelation = self::getDocsByVisibleRange($date1, $date2, $visible) / self::allDocsByRange($date1, $date2)*100;
        return round($totalRelation, 2);
    }






















}
