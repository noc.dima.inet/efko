<?php


use Illuminate\Http\Request;
use App\Http\Controllers\ {
    IndexController,
    ImageController,
    StoreImageController,
    StatImageController,
    LoginController,
    LogoutController,
    RegisterController,
    RegistrationController,
    PrivateController,
};
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
*/

Route::get('/', IndexController::class)->name('index');

Route::get('/registration', RegistrationController::class)->name('registration');
Route::post('/registration', RegisterController::class);

Route::controller(LoginController::class)->group(function () {
    Route::get('/login', 'show')->name('login');
    Route::post('/login', 'login');
});

Route::middleware('auth')->group(function(){
    Route::post('/store-image', StoreImageController::class)->middleware('auth')->name('store-image');
    Route::post('/stat-image', StatImageController::class)->middleware('auth')->name('stat-image');
    Route::get('/stat', StatImageController::class)->middleware('auth')->name('stat');
    Route::get('/image', ImageController::class)->middleware('auth')->name('image');
    Route::get('/private', PrivateController::class)->middleware('auth')->name('private');
});

Route::get('/logout', LogoutController::class)->name('logout');
