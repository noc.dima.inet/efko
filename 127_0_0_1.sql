-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Окт 22 2022 г., 20:23
-- Версия сервера: 5.7.39
-- Версия PHP: 8.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `efko`
--
CREATE DATABASE IF NOT EXISTS `efko` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `efko`;

-- --------------------------------------------------------

--
-- Структура таблицы `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `visible` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'PU',
  `date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `images`
--

INSERT INTO `images` (`id`, `user_id`, `image`, `visible`, `date`, `created_at`, `updated_at`) VALUES
(10, 1, 'uploads/B28oYVm4xrerhsb0duP8HRucdaUQV80PsidDHh89.jpg', 'PU', '2018-12-05', '2022-10-19 03:37:37', NULL),
(11, NULL, 'uploads/YpHhxzyag48cppr71JZf0Zt6OmYsOXfwgQUXXyl4.jpg', 'PU', '2022-10-19', '2022-10-18 03:38:58', NULL),
(12, NULL, 'uploads/T4aFqBdYK2LGaUjcySv1v7IGBdbc17q30W5jGcsb.jpg', 'PU', '2022-09-07', NULL, NULL),
(13, NULL, 'uploads/c3SLXUffU8qmL7beYR97ypJgj22nrmcAddq1L50x.jpg', 'AU', '2022-01-15', NULL, NULL),
(14, NULL, 'uploads/W6MmshDw3YsqLWrZanHKVLqnvZhilwqzSWqgcEbC.jpg', 'PU', '2022-10-20', NULL, NULL),
(15, NULL, 'uploads/oxjjWVbyWeQU2JoUu1KpCWE4iEOnCeYsFfZv3nNm.jpg', 'AU', '2022-10-18', NULL, NULL),
(16, NULL, 'uploads/WtcRZpI4TG5mq2iXdKGSxfvWsvcTwcYPmAr57oml.jpg', 'PO', '2022-09-04', NULL, NULL),
(17, NULL, 'uploads/M5Kr625MLO4sVmMT69g3NUucEsPo2dlscbXt6U6H.jpg', 'PO', '2022-10-18', NULL, NULL),
(18, NULL, 'uploads/A7JExW3Hy7GweDLWGleEZMY2fCzUW0mZZPhSxZZx.jpg', 'PU', '2022-09-19', '2022-10-20 06:59:23', NULL),
(19, NULL, 'uploads/RlTcYM1tlAoHpKxnCa7Fw4dnnbikvmWnFrUubt62.jpg', 'PU', '2021-10-20', '2022-10-20 06:59:31', NULL),
(20, NULL, 'uploads/diZJIICCS36ay3FJDo3AwoLTGYTyv3BbclEi9EzV.jpg', 'PU', '2022-10-17', '2022-10-21 06:02:35', NULL),
(21, NULL, 'uploads/0KohwgJqSOBKruxfhdeu6lPo3PxLMC4eLVjWaw6S.jpg', 'AU', '2022-10-21', '2022-10-21 07:00:41', NULL),
(22, NULL, 'uploads/tkjekWu3kk8lCzHtnew021mWTkpAogdORXHmPfmb.jpg', 'AU', '2022-10-21', '2022-10-21 16:07:42', NULL),
(23, NULL, 'uploads/zlMKoP3Dbs1PrP6nIiaAwrjCoFVTpGajV5IBQDEh.jpg', 'PU', '2022-10-21', '2022-10-21 16:07:54', NULL),
(24, NULL, 'uploads/gyPiwIyEEoI7oh2Ex5JFmEgrbwveVaI2wHldnNXb.jpg', 'PO', '2022-10-22', '2022-10-22 11:30:49', NULL),
(25, NULL, 'uploads/4vDMxSXFxdzidt90bbm1faMZuQbycQ2v4gjRcsIN.jpg', 'AU', '2022-10-22', '2022-10-22 11:32:18', NULL),
(26, NULL, 'uploads/JkETWdHMWQ0RJxU27IOFnZ337LJ8GgP5x93cQMog.jpg', 'PU', '2022-10-22', '2022-10-22 11:37:49', NULL),
(27, NULL, 'uploads/oZEZVGCp33Obuoz1C8EpVKS9OAwQwucCW8gZQPAc.png', 'PU', '2022-10-22', '2022-10-22 14:06:02', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_10_19_054907_create_images_table', 1),
(6, '2022_10_19_093359_update_images_table', 2),
(7, '2022_10_22_050505_create_users_table', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(22, 'admin', 'admin@yandex.ru', '$2y$10$mUhIlzN0IDxEUYsCZI0waObh5Av6mh8bTT0VFWKHDn/Au.NgpSL22', NULL, '2022-10-22 12:37:41', '2022-10-22 12:37:41'),
(23, 'alexis', 'al@yandex.ru', '$2y$10$bLurIW2ayt4C15gv0BpYp.ZN6YJuMJS7.9ZBMl1KhRmogD97p0d6m', NULL, '2022-10-22 12:38:52', '2022-10-22 12:38:52'),
(24, 'user1', 'user1@yandex.ru', '$2y$10$ggwf.UrbF8MdEN4WbEsB5OWHWHgrk5FuSkGqhnwn8GaM/DLB3eVMW', NULL, '2022-10-22 13:33:48', '2022-10-22 13:33:48'),
(25, 'valliko', 'Valliko@uuu.ru', '$2y$10$ioZn.fKFfnLRIDxTk8pj0ux8ysVuCJa02QmyltnNBfU06cyagh.GG', NULL, '2022-10-22 14:05:37', '2022-10-22 14:05:37'),
(26, 'maggi', 'maggi@yandex.ru', '$2y$10$AELW0zWyDQHBslfKJ1oa8u3yV18iTysztO34mrgggGzNg4.xR5Zjy', NULL, '2022-10-22 14:16:09', '2022-10-22 14:16:09');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_name_unique` (`name`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
