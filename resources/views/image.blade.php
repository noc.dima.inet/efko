@extends('layout')

@section('content')
<p><a href="{{ route('index') }}" title="">Главная</a> &nbsp; | 
    <a href="{{ route('private') }}" title="">Вернуться</a> &nbsp; | 
    &nbsp;<a href="{{ route('logout') }}" title="">Выйти</a> &nbsp;
</p>
<hr>
    <h1>Загрузить картинку</h1>
    <div style="">
        <form action='{{ route('store-image') }}' method='post' enctype="multipart/form-data">
            {{ csrf_field() }}

            <div>
                <input type='file' name='image' />
            </div>
            <div>
                <select name='visible'>
                    <option value='PU'>Public</option>
                    <option value='AU'>Authorize</option>
                    <option value='PO'>Private</option>                   
                </select>
            </div>
            <br>
            <div>
                <button type='sumbit' >Загрузить</button>
            </div>
        
        </form>
		
		<hr>
 
		
		
	</div>
    <br>
    <div  >
        <p>
            <b><a href="{{ route('private') }}" title="">вернуться</a></b>
        </p>
    </div>
@endsection