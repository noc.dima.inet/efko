@extends('layout')

@section('content')
<p><a href="{{ route('index') }}" title="">Главная</a> &nbsp; |  &nbsp; <a href="{{ route('login') }}" title="">Войти</a></p>
<hr>
<h1>Регистрация для пользователей</h1>
	
<div >
    <form action="{{ route('registration') }}" method="POST">
        {{ csrf_field() }}
        <input type="text" id="name" name="name" placeholder="Введите уникальный ник"> <br>
        @error('name')
            <p style="color:red">{{ $message }}</p>
        @enderror
        <br>
        <input type="text" id="email" name="email" placeholder="Введите email"> <br>
        @error('email')
            <p style="color:red">{{ $message }}</p>
        @enderror
        <br>
        <input type="password" id="password" name="password" placeholder="Введите пароль"><br>
        @error('password')
        <p style="color:red">{{ $message }}</p>
        @enderror
        <br>
        <button type='sumbit' >Регистрация</button>

    </form>
</div>


 

@endsection