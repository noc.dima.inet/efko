@extends('layout')

@section('content')
    <a href="{{ route('private') }}" title="">Вернуться</a> &nbsp; | &nbsp; 
     <a href="{{ route('logout') }}" title="">Выход</a> &nbsp;
 </p>
    <h1>Статистика</h1>
    <div >
         <p>По Вашему запросу на период <b> с {{ $dateFrom }} по {{ $dateTo  }} </b>всего доков: {{ $allDocs }}</p>
		<p>
            Соотношение к общему за даный период:<br>
              публичных: {{ $totalRelationPU }}% <br>
              приватных: {{ $totalRelationPO }}% <br>
              только д. авторизованных: {{ $totalRelationAU }}% <br>
        </p>
		
	</div>
    <br>
    <div >
        <p>
            <b><a href="{{ route('private') }}" title="">вернуться</a></b>
        </p>
    </div>
@endsection