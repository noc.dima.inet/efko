<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Эфко тестовое</title>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script src = "../js/jquery/i18n/datepicker-ru.js" ></script>
    <script src = "../js/jquery/jquery.js" >  </script>

    <script type="text/javascript">
        jQuery(document).ready(function(){
            $.datepicker.setDefaults( $.datepicker.regional[ "ru" ] );
            $( "#datepicker1,#datepicker2" ).datepicker({
                dateFormat : "yy-mm-dd",
                minDate: new Date($('#hiddendelivdate').val()),
                monthNames : ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                dayNamesMin : ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
                firstDay: 1,
            });
        } );
    </script>


</head>
<body>
    @yield('content')
</body>
</html>