@extends('layout')

@section('content')
<p><a href="{{ route('index') }}" title="">Главная</a> &nbsp; |  &nbsp; <a href="{{ route('registration') }}" title="">Регистрация</a></p>
<hr>
<h1>Вход для пользователей</h1>
	
<div >
    <form action="{{ route('login') }}" method="POST">
        {{ csrf_field() }}
        <input type="text" id="email" name="email" placeholder="Введите свой e-mail"> <br>
        @error('email')
            <p style="color: red">{{ $message }}</p>
        @enderror
        <br>
        <input type="password" id="password" name="password" placeholder="Введите Пароль"><br>
        @error('password')
            <p style="color: red">{{ $message }}</p>
        @enderror
        <br>
        <button type='sumbit' >Войти</button>

    </form>
</div>


 

@endsection