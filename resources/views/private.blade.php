@extends('layout')

@section('content')
<p><a href="{{ route('index') }}" title="">Главная</a> &nbsp; | &nbsp; 
    <a href="{{ route('logout') }}" title="">Выход</a> &nbsp;
</p>
<h1>Привет, {{ $name }}</h1>
<div >
    <p>Просмотр доступных картинок</p>
		
		
    <p>
        <a href={{ route('image') }} title="страница загрузки картинки">Загрузить картинку</a>
    </p>
<hr>
<p>Кол-во доков за текущий: 
    день: <b> {{ $countImgByDay }} </b> | 
    месяц: <b> {{ $countImgByMonth }} </b> | 
    год:  <b> {{ $countImgByYears }} </b> | 
</p>
<hr>
    
<p>Статистика по документам за период: 

</p>		
    <div >
        <form action="{{ route('stat-image') }}" method="POST">
            {{ csrf_field() }}
            <p>c &nbsp;<input type="text" id="datepicker1" name="dateFrom"> &nbsp; - &nbsp;
                по &nbsp;<input type="text" id="datepicker2" name="dateTo"></p>	
            <button type='sumbit' >Показать</button>

        </form>
    </div>

</div>

<hr>
<div >
    <h3>Фото ограниченного доступа</h3>

   @foreach ($privateImages as $image)
    <div style='margin:4px; float:left; '>
        <img style='height:150px' src="./{{ $image }}" alt="">
    </div>
    @endforeach

</div>
<div style="margin: 10px; clear:both"></div>
<div>
    <h3>Фото из публичного доступа</h3>

    @foreach ($publicImages as $image)
    <div style='margin:4px; float:left; '>
        <img style='height:150px' src="./{{ $image }}" alt="">
    </div>
    @endforeach

</div>



@endsection