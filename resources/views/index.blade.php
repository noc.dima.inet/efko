@extends('layout')

@section('content')
<h1>Привет!</h1>
@if (Auth::check())  
    <p><a href="{{ route('logout') }}" title="">Выход</a> &nbsp; |  &nbsp; <a href="{{ route('private') }}" title="">Приватная страница</a> &nbsp; </p>
@else
    <p><a href="{{ route('login') }}" title="">Войти</a> &nbsp; |  &nbsp; <a href="{{ route('registration') }}" title="">Регистрация</a></p>
@endif 
<div >
       
<div style="margin: 10px; clear:both"></div>
<div >
    <h3>Фото из публичного доступа</h3>

    @foreach ($publicImages as $image)
    <div style='margin:4px; float:left; '>
        <img style='height:150px' src="./{{ $image }}" alt="">
    </div>
    @endforeach
</div>

</div>
<div style="margin: 10px; clear:both"></div>


@endsection